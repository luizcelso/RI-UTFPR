#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 18 08:53:20 2017

@author: luizcelso
"""

import requests
from bs4 import BeautifulSoup

page = requests.get("http://dainf.ct.utfpr.edu.br/~gomesjr/IR/extraction/teste_css.html")

print("Original contents:")
print page.content

soup = BeautifulSoup(page.content, 'html.parser')

print("Pretty contents:")
print(soup.prettify())

print("All paragraphs:")
print(soup.find_all('p'))

print("All CopyRight (copy) paragraphs:")
print(soup.find_all('p', class_='copy'))

print("All CopyRight (copy) paragraphs, just the content:")
print(soup.find_all('p', class_='copy')[0].get_text())

print("Get elements by id:")
print(soup.find_all(id="link01"))


