#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 29 08:54:02 2017

@author: luizcelso
"""
import itertools
import pandas as pd
import matplotlib.pyplot as plt

import numpy as np
from sklearn.svm import SVC
from sklearn.naive_bayes import MultinomialNB
from sklearn.utils import shuffle
from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer



home_dir = '/home/luizcelso/Dropbox/utfpr/teaching/IR/201702/trabalho/corpus/'

docs_file = 'Corpus de notícias.xlsx'


df_docs_original = pd.read_excel(home_dir + docs_file)


df_docs = df_docs_original.drop_duplicates(u"Link para a notícia")

df_docs = df_docs[[u"Link para a notícia", u"Texto da notícia", u"Categoria da notícia"]]

df_docs = df_docs.set_index(u"Link para a notícia")

data = df_docs.rename(columns={u"Texto da notícia":"text", u"Categoria da notícia":"class"})

data = shuffle(data)

labels = data["class"].unique().tolist()

#pipeline = Pipeline([
#    ('vectorizer',  TfidfVectorizer()),
#    ('classifier',  MultinomialNB()) ])


pipeline = Pipeline([
    ('vectorizer',  CountVectorizer()),
    ('classifier',  SVC(kernel='linear', C=0.01)) ])


#pipeline.fit(data['text'].values, data['class'].values)


def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else '.0f'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')



from sklearn.cross_validation import KFold
from sklearn.metrics import confusion_matrix, f1_score


k_fold = KFold(n=len(data), n_folds=4)
scores = []

confusion =  np.zeros((len(labels),len(labels)))

for train_indices, test_indices in k_fold:
    train_text = data.iloc[train_indices]['text'].values
    train_y = data.iloc[train_indices]['class'].values

    test_text = data.iloc[test_indices]['text'].values
    test_y = data.iloc[test_indices]['class'].values

    pipeline.fit(train_text, train_y)
    predictions = pipeline.predict(test_text)

    confusion += confusion_matrix(test_y, predictions, labels=labels)
    score = f1_score(test_y, predictions, average="micro", labels=labels)
    scores.append(score)


print('Total classified:', len(data))
print('Score:', sum(scores)/len(scores))
print('Scores:', scores)


np.set_printoptions(precision=2)

# Plot non-normalized confusion matrix
plt.figure(figsize=(15,15))
plot_confusion_matrix(confusion, classes=labels, 
                      title='Confusion matrix, without normalization')

# Plot normalized confusion matrix
plt.figure(figsize=(15,15))
plot_confusion_matrix(confusion, classes=labels, normalize=True,
                      title='Normalized confusion matrix')

plt.show()

# Following lines show how to make new predictions with the trained model

examples = ["""A Justiça decidiu manter preso preventivamente o casal apontado como autor da morte do menino de 6 anos que desapareceu na manhã de sexta-feira (25) e foi encontrado no começo da tarde do mesmo dia no bairro Jardim Bonfim, em Almirante Tamandaré, Região Metropolitana de Curitiba (RMC).A casa em que o casal morava foi destruída na noite de sexta-feira, por moradores que se revoltaram com o que os dois fizeram
            O casal, cuja mulher era babá da criaça, foi preso ainda durante o começo das investigações na busca pelo garoto. Horas depois, a mulher confessou o crime contando inclusive detalhes de como tudo aconteceu. O marido, segundo a suspeita da polícia, teve participação no assassinato.
       """, """
       Ladrões não mediram as consequências e, por pouco, não causaram um estrago ainda maior ao explodir um caixa eletrônico em Campo Magro, na região metropolitana de Curitiba. Isso porque o grupo detonou explosivos na máquina que funciona dentro de um posto de combustíveis na Estrada do Cerne.
       A ação ocorreu por volta das 4h da madrugada desta segunda-feira (28). O valor levado pelos ladrões não foi informado pela polícia.
       O posto roubado fica em frente à prefeitura de Campo Magro. Testemunhas contaram que seis pessoas participaram do ataque. Eles chegaram num Honda Civic, e estavam fortemente armados.
       ""","""
       O Detran do Paraná vai capacitar a Guarda Municipal de Curitiba para fiscalização do trânsito. As aulas da primeira turma, com 51 alunos, começaram nesta segunda-feira. A parceria 
       ""","""
       Saúde, hospital, atendimento, poluição
       ""","""
             Ladrões não mediram as consequências e, por pouco, não causaram um estrago ainda maior ao explodir um caixa eletrônico em Campo Magro, na região metropolitana de Curitiba. Isso porque o grupo detonou explosivos na máquina que funciona dentro de um posto de combustíveis na Estrada do Cerne.
       A ação ocorreu por volta das 4h da madrugada desta segunda-feira (28). O valor levado pelos ladrõesparam do ataque. Eles chegaram num Honda Civic, e estavam fortemente armados.
       """
       ]

predictions = pipeline.predict(examples)
print predictions # [1, 0]