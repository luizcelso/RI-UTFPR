#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 29 08:54:02 2017

@author: luizcelso
"""
import urllib
from bs4 import BeautifulSoup
import re
import lxml.html.clean
import nltk
import pandas as pd

import numpy
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.naive_bayes import MultinomialNB



home_dir = '/home/luizcelso/Dropbox/utfpr/teaching/IR/201702/trabalho/corpus/'

docs_file = 'Corpus de notícias.xlsx'


df_docs_original = pd.read_excel(home_dir + docs_file)


df_docs = df_docs_original.drop_duplicates(u"Link para a notícia")

df_docs = df_docs[[u"Link para a notícia", u"Texto da notícia", u"Categoria da notícia"]]

df_docs = df_docs.set_index(u"Link para a notícia")

data = df_docs.rename(columns={u"Texto da notícia":"text", u"Categoria da notícia":"class"})

count_vectorizer = CountVectorizer()

counts = count_vectorizer.fit_transform(data['text'].values) #learns the vocabulary of the corpus and extracts word count features.

classifier = MultinomialNB()
targets = data['class'].values
classifier.fit(counts, targets)

examples = ["""A Justiça decidiu manter preso preventivamente o casal apontado como autor da morte do menino de 6 anos que desapareceu na manhã de sexta-feira (25) e foi encontrado no começo da tarde do mesmo dia no bairro Jardim Bonfim, em Almirante Tamandaré, Região Metropolitana de Curitiba (RMC).A casa em que o casal morava foi destruída na noite de sexta-feira, por moradores que se revoltaram com o que os dois fizeram
            O casal, cuja mulher era babá da criaça, foi preso ainda durante o começo das investigações na busca pelo garoto. Horas depois, a mulher confessou o crime contando inclusive detalhes de como tudo aconteceu. O marido, segundo a suspeita da polícia, teve participação no assassinato.
       """, """
       Ladrões não mediram as consequências e, por pouco, não causaram um estrago ainda maior ao explodir um caixa eletrônico em Campo Magro, na região metropolitana de Curitiba. Isso porque o grupo detonou explosivos na máquina que funciona dentro de um posto de combustíveis na Estrada do Cerne.
       A ação ocorreu por volta das 4h da madrugada desta segunda-feira (28). O valor levado pelos ladrões não foi informado pela polícia.
       O posto roubado fica em frente à prefeitura de Campo Magro. Testemunhas contaram que seis pessoas participaram do ataque. Eles chegaram num Honda Civic, e estavam fortemente armados.
       ""","""
       O Detran do Paraná vai capacitar a Guarda Municipal de Curitiba para fiscalização do trânsito. As aulas da primeira turma, com 51 alunos, começaram nesta segunda-feira. A parceria 
       """
       ]
example_counts = count_vectorizer.transform(examples)
predictions = classifier.predict(example_counts)
print predictions # [1, 0]